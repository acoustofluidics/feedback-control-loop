import time
import cv2
import matplotlib.pyplot as plt
import numpy as np
from PIL import ImageGrab
from datetime import datetime, date
import os


def get_time():
    now = datetime.now()

    today = date.today()
    current_time_min = now.strftime("%H-%M")
    current_time_sec = now.strftime("%H-%M-%S")
    time_i = today.strftime("%b-%d-%Y")
    return now, today, current_time_min, current_time_sec, time_i


def create_directories(working_directory):
    now, today, current_time_min, current_time_sec, time_i = get_time()

    # create main directory including date and time of start
    directory = str(time_i) + '_' + str(current_time_min)
    parent_dir = working_directory
    path = os.path.join(parent_dir, directory)
    os.mkdir(path)

    # create subdirectories for phase 1 and phase 2
    os.mkdir(path + r'\phase1')
    os.mkdir(path + r'\phase2')
    phase2 = path + r'\phase2'
    os.mkdir(phase2 + r'\images')
    os.mkdir(phase2 + r'\plots')
    os.mkdir(phase2 + r'\data')

    # current working folder location
    working_directory += "\\" + directory + "\\"
    folder_1 = working_directory + r'\phase1' + "\\"  # folder for phase 1
    folder_img = working_directory + r'\phase2\images' + "\\"  # folder for images phase 2
    folder_plt = working_directory + r'\phase2\plots' + "\\"  # folder for plots phase 2
    folder_data = working_directory + r'\phase2\data' + "\\"  # folder for data phase 2

    return working_directory, folder_1, folder_img, folder_plt, folder_data


def initialise_frequency(perc, width):
    """
    initialises the starting frequency at f_opt-percentage
    :param perc: percentage range in which we want to check for the optimal frequency
    :param width: channel width
    :return: returns the theoretically optimal frequency f_opt
    """
    assert 0 <= width <= 2000
    freq_opt = 1500 / (width * 2e-6)
    print("optimal frequency would be:", int(freq_opt), "Hertz")
    freq_min = int((1 - perc) * freq_opt)
    freq_max = (1 + perc) * freq_opt
    return freq_min, freq_max, freq_opt


def check_AFG(ser, port):
    """
    The purpose of this function is to test if the specified port and AFG have been found and if we can
    establish a connection with the given AFG

    :param ser: serial AFG
    :param port: which port is used, can be verified in the device manager or in the AFG software
    :return: boolean, depending on if the connection works or not
    """

    # Starts the driver and establishes the connection to the camera
    if ser.is_open:
        print("Connection to the AFG established")
    else:
        print("ERROR, the port is not open")
        return False

    #  Controls if the connected port is the specified
    if ser.port == port:
        print("You connected the AFG with port", ser.name, "which is the one you specified:", port)
    else:
        print("ERROR, somehow the connection failed")
        return False

    time.sleep(0.5)
    return True


def initialise_AFG(ser):
    """
    starts the AFG (remote control ON, Display OFF and Output 1 ON)
    :param ser: connected AFG
    :return: NONE
    """
    ser.write(b'SYST:REM\n')
    time.sleep(0.1)
    ser.write(b'DISPlay OFF\n')
    time.sleep(0.1)
    """ser.write(b'SOUR1:VOLT:UNIT VPP 0.5\n')
    time.sleep(0.1)"""
    ser.write(b'OUTP1 ON\n')
    time.sleep(0.1)

    return


def check_camera(ser_numb, index=1):
    """
    The purpose of this function is to check if we can establish a connection with the connected uEYE camera,
    and if we can access the camera information

    :param ser_numb: serial number of the camera
    :param index: which port is the camera connected to.
    :return: True if the connection was established and everything works
    """
    cam = ueye.HIDS(index)
    cInfo = ueye.CAMINFO()

    #  Starts the driver and establishes the connection to the camera
    connect = ueye.is_InitCamera(cam, None)
    if connect != ueye.IS_SUCCESS:
        print("ERROR, Connection to camera failed")
        return False
    else:
        print("Connection to camera:", cam, "established")

    #  Checks if the Camera-Information can be accessed.
    info = ueye.is_GetCameraInfo(cam, cInfo)
    if info != ueye.IS_SUCCESS:
        print("ERROR, camera information could not be accessed")
        return False
    else:
        print("Camera information accessed")

    #  Controls if the connected camera is the specified
    ser_numb_cam = cInfo.SerNo.decode('utf-8')
    if ser_numb == ser_numb_cam:
        print('Success, this is the desired camera')
    else:
        print('ERROR, There was a mismatch in camera allocation')
        return False

    #  required to release the driver and allow openCV (cv2) to work
    ueye.is_ExitCamera(cam)
    return True


def set_frequency(ser, freq, form="SIN"):
    """
    Sets the AFG to the specified frequency (if simulation = FALSE)
    Else it simulates the change of frequency by printing the current freq

    :param ser: connected AFG
    :param freq: frequency we want to apply
    :param form: default sine-wave
    :return: NONE
    """

    command = "SOUR1:APPL:" + form + " " + str(freq) + "\n"
    command_bytes = bytes(command, 'utf-8')
    ser.write(command_bytes)
    time.sleep(0.02)

    ser.write(b'SOURce1:FREQuency?\n')
    freq = ser.readline().decode('utf-8')
    print("The AFG changes the frequency to (Hertz): " + str(freq))

    return


def get_background(background, timedelay=1, rounds=5):
    """
    This function calculates an averaged background image.
    :param cap: capture of the live camera
    :param rounds: how often the the image should be averaged (3 standard value), duration increases one second/round
    :param timedelay: timedelay between two consecutive frames, default value is 1 sec
    :return: the averaged background image
    """
    result = np.array(background) / rounds

    for i in range(1, rounds):
        time.sleep(timedelay)
        img = np.array(ImageGrab.grab(bbox=None, all_screens=True, xdisplay=None))[:, :, 0]
        temp = np.array(img) / rounds
        result = np.add(result, temp)

    background_img = np.array(result.astype(int), dtype=np.uint8)
    return background_img


def get_avg_pixel(img):
    """
    Computes the average of pixel intensity over the length (x-Axis) of a Frame for the entire width (y-Axis)

    :param img: Image consisting of integer values between [0, 255]
    :return: A np.array with as many entries as the width with the averaged
    pixel intensities over the length of the image
    """
    width, length = img.shape
    result = np.empty(width)
    for i in range(width):
        result[i] = np.mean(img[i, :])
    return result


def get_linewidth(arr):
    """
    Returns the linewidth of an array/function by counting all values, that lie above the mean of the given values

    :param arr: Array of which linewidth is to be calculated
    :return: Linewidth
    """
    count = 0
    mean = np.mean(arr)
    for i in range(len(arr)):
        if arr[i] > mean:
            count += 1
    return count


def get_data_1(region, freq, background_img, data, best_linewidth, best_freq, subtraction):
    """
    This function computes a background subtraction, crops the input image to the ROI, calculates the linewidth and
    stores all the data in a matrix. Furthermore it returns the best=narrowest linewidth respectively the
    corresponding best frequency

        :param region: ROI, region of interest
        :param freq: currently applied frequency
        :param background_img: background image
        :param data: data matrix with so far stored values
        :param best_linewidth: narrowest linewidth so far
        :param best_freq: frequency corresponding to the narrowest linewidth so far
        :param subtraction: should a background subtraction be conducted or not
        :return: the updated data matrix
        """

    img = np.array(ImageGrab.grab(bbox=None, all_screens=True, xdisplay=None))[:, :,
          0]  # choose ROI from screen capture
    img = np.array(img.astype(int), dtype=np.int16)
    if subtraction:
        img = np.absolute(img - background_img)
    img = np.array(img.astype(int), dtype=np.uint8)

    y_start, y_end, x_start, x_end = region
    img = img[y_start:y_end, x_start:x_end]

    data = np.roll(data, 1, axis=0)
    linewidth = get_linewidth(get_avg_pixel(img))
    data[0] = [linewidth, freq]

    if linewidth <= best_linewidth:
        best_freq = freq
        best_linewidth = linewidth

    return data, best_freq, best_linewidth


def plot_linewidths(data_a, data_b, folder):
    """
    This function plots the linewidths corresponding to the respective frequencies, and stores both the image and the data in the specified path
    :param data: data matrix, where all the linewidths and frequencies are stored
    :param folder: location where to save the plot and data
    :return: None, only a plot is manufactured
    """
    now, today, current_time_min, current_time_sec, time_i = get_time()


    x_axis_a = data_a[:, 1]
    y_axis_a = data_a[:, 0]
    plt.plot(x_axis_a, y_axis_a, label="upwards iteration")
    x_axis_b = data_b[:, 1]
    y_axis_b = data_b[:, 0]
    plt.plot(x_axis_b, y_axis_b, label="downwards iteration")


    plt.title('Phase 1')
    plt.xlabel("Frequency in [Hz]")
    plt.ylabel("Linewidth in [pixel]")
    # export figure to .png file
    folder_plot = folder + 'plot_at_' + str(current_time_sec) + '.png'
    plt.legend()
    plt.savefig(folder_plot)
    plt.show()

    # export data points to .txt file
    folder_data = folder + 'data_at_' + str(current_time_sec) + '.txt'
    np.savetxt(folder_data, data_a, delimiter=",")
    np.savetxt(folder_data, data_b, delimiter=",")

    return


def get_data_2(region, freq, background_img, data, subtraction):
    """
    This function computes a background subtraction, crops the input image to the ROI, calculates the linewidth and
    stores all the data in a matrix.

    :param region: ROI, region of interest
    :param freq: currently applied frequency
    :param background_img: background image
    :param data: data matrix with so far stored values
    :param subtraction: should a background subtraction be conducted or not
    :return:
    """

    img = np.array(ImageGrab.grab(bbox=None, all_screens=True, xdisplay=None))[:, :, 0]  # choose ROI from screen capture
    img = np.array(img.astype(int), dtype=np.int16)
    if subtraction:
        img = np.absolute(img - background_img)
    img = np.array(img.astype(int), dtype=np.uint8)

    y_start, y_end, x_start, x_end = region
    img = img[y_start:y_end, x_start:x_end]

    data = np.roll(data, 1, axis=0)
    result = get_avg_pixel(img)
    linewidth = get_linewidth(result)
    data[0, 0:-2] = result
    data[0, -2] = linewidth
    data[0, -1] = freq

    return data, linewidth


def plot_brightness(data, size, threshold, folder_plot, folder_data):
    """
    This function plots the averaged brightness of the entire channel width for the (at most) last 12 entries.
    Furthermore it stores the data and the plot in a separate directory.

    :param data: data matrix with information
    :param size: size of data matrix, in order to plot relevant numbers only
    :param threshold: threshold value
    :param folder_plot: path where to save plot
    :param folder_data: path where to save data

    :return: None, just a plot is manufactured, and the corresponding data & plot are stored
    """
    now, today, current_time_min, current_time_sec, time_i = get_time()
    plt.close('all')
    plt.figure(figsize=(15, 8))
    x_axis = np.arange(0, (len(data[0]) - 2))
    for index in range(0, size, threshold):
        y_axis = data[index, :-2]
        plt.plot(x_axis, y_axis,
                 label="f = {} [Hertz]; Linewidth of {} [pixel]".format(int(data[index, -1]), int(data[index, -2])))
    plt.xlabel("Height of region of interest (ROI) in [pixel]")
    plt.ylabel("Brightness in [float]")
    plt.title('Phase 2')
    plt.legend(loc='best', bbox_to_anchor=(1, 1), ncol=1, fancybox=True, shadow=True)
    # export figure to .png file
    folder_plot += 'plot_at_' + str(current_time_sec) + '.png'
    plt.savefig(folder_plot, bbox_inches='tight')
    plt.show(block=False)
    plt.pause(0.001)

    # export data points to .txt file
    folder_data += 'data_at_' + str(current_time_sec) + '.txt'
    np.savetxt(folder_data, data, delimiter=",")

    return


def display_img(region, background_img, folder_img):
    """
    This function displays the original image in the upper half and the background-subtracted image in the lower half
    of the screen

    :param region: ROI chosen before :param background_img: background image for subtraction process
    :param background_img: background image for background subtraction purposes
    :param folder_img: path where to save image

    :return: None, only a pop-up window
    """
    cv2.destroyAllWindows()
    now, today, current_time_min, current_time_sec, time_i = get_time()

    img_orig = np.array(ImageGrab.grab(bbox=None, all_screens=True, xdisplay=None))[:, :, 0]  # choose ROI from screen capture
    img = np.array(img_orig.astype(int), dtype=np.int16)
    img_back = np.absolute(img - background_img)

    y_start, y_end, x_start, x_end = region
    img_orig = img_orig[y_start:y_end, x_start:x_end]
    img_back = img_back[y_start:y_end, x_start:x_end]
    width, length = img_orig.shape

    disp = np.full((2 * width + 3, length), 255)

    disp[0:width] = img_orig
    disp[(width + 3):] = img_back
    disp = np.array(disp.astype(int), dtype=np.uint8)
    disp_size = cv2.resize(disp, (400, 480))
    cv2.imshow("Upper image: original live; Lower image: live with background subtraction, press q to quit", disp_size)
    folder_img += 'image_at_' + str(current_time_sec) + '.png'
    cv2.imwrite(folder_img, disp)
    cv2.waitKey(1)

    return


def save_to_file(fullpath, now_0, now_1, now_2, now_q, best_freq, freq, freq_min, freq_max, freq_opt,
                 initial_time_delay_1, frequency_step_size_1, time_delay_1, wait_time_transition_2, frequency_step_size_2, time_delay_2, threshold, swing_value,
                 freq_bound, subtraction, particle_size, device,  flow_rate):
    """
    This function exports the variables used in the program to a .txt file
    :param fullpath: file save location and file name
    :param now: time + date when starting the test
    :param now_1: time + date when closing the test
    :param time_delta: time it took for the test to run
    :param best_freq: best frequency found in phase 1
    :param freq: last set frequency phase 2
    :param freq_min: lowest frequency tested
    :param freq_max: highest frequency tested
    :param freq_opt: theoretical optimal frequency
    :param subtraction: should a background subtraction be conducted or not

    ....
    all parameters are self explanatory by reading the output text below in the code.

    :return: None
    """

    with open(fullpath, "w") as f:
        f.writelines(f"Script start: {now_0.strftime('%H:%M:%S')}\n"
                     f"Device Information: {device} \n"
                     f"Particle size used is: {particle_size} um\n"
                     f"Flow rate used is: {flow_rate} ul/min\n"
                     f"Lowest frequency tested: {freq_min} Hz\n"
                     f"Highest frequency tested: {freq_max} Hz\n"
                     f"Theoretical optimal frequency lambda/2: {freq_opt} Hz\n"
                     f"Image subtraction: {subtraction}\n \n")

        f.writelines(f"Phase 1: \n"
                     f"     -Start: {now_1.strftime('%H:%M:%S')}\n"
                     f"     -Time delay for system to settle [s]: {initial_time_delay_1}\n"
                     f"     -Step size [Hz]: {frequency_step_size_1}\n"
                     f"     -Time delay [s]: {time_delay_1}\n"
                     f"     -Best frequency found in phase 1: {best_freq} Hz\n\n")

        f.writelines(f"Phase 2 \n"
                     f"     -Start: {now_2.strftime('%H:%M:%S')}\n"
                     f"     -Time delay for system to settle [s]: {wait_time_transition_2}\n"
                     f"     -Step size [Hz]: {frequency_step_size_2}\n"
                     f"     -Time delay [s]: {time_delay_2}\n"
                     f"     -Threshold value for change of direction [ ]: {threshold}\n"
                     f"     -Swing value, value with which the stepsize is demagnified [ ]: {swing_value}\n"
                     f"     -Lower Frequency boundary for minimal step size [Hz]: {freq_bound}\n"
                     f"     -Last Set Frequency Phase 2 (equals the found excitation Frequency): {freq} Hz\n\n")

        f.writelines(f"Script stop: {now_q.strftime('%H:%M:%S')}\n")
        f.writelines(f"Elapsed time : {now_q-now_0}\n")
