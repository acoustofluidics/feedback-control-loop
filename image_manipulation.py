import cv2


cropping, x_start, y_start, x_end, y_end = None, None, None, None, None


def mouse_crop(event, x, y, flags, param):
    """
    mouse_crop() is called by choose_frame() and saves the x and y coordinates from the rectangle which is to be cropped

    :param event: tells the function if mouse is clicked or not, and where the mouse cursor is right now
    :param x: x-coordinate of the mouse
    :param y: y-coordinate of the mouse
    :param flags: -
    :param param: handing the original image to the function, to display the cropped region
    :return: None, the coordinates are transmitted via global variables
    """
    ori_image = param
    global cropping, x_start, y_start, x_end, y_end
    if event == cv2.EVENT_LBUTTONDOWN:
        x_start, y_start, x_end, y_end = x, y, x, y
        cropping = True
    elif event == cv2.EVENT_MOUSEMOVE:
        if cropping:
            x_end, y_end = x, y
    elif event == cv2.EVENT_LBUTTONUP:
        x_end, y_end = x, y
        cropping = False
        roi = ori_image[y_start:y_end, x_start:x_end]
        cv2.imshow("Press 'r' to reframe, press 'c' to save and exit", roi)


def choose_frame(image):
    """
    choose_frame() is called so one can graphically choose the region for which the bandwidth shall be determined. The
    code is adapted from 'https://www.pyimagesearch.com/2015/03/09/capturing-mouse-click-events-with-python-and
    -opencv/'
    internally the function mouse_crop is called.

    :param image: image from which we want to select the region
    :return: the x- and y-values we want to crop all the following frames
    """

    global cropping, x_start, y_start, x_end, y_end
    cropping = False
    x_start, y_start, x_end, y_end = 0, 0, 0, 0
    ori_image = image.copy()

    cv2.namedWindow("image", cv2.WINDOW_NORMAL)
    cv2.setMouseCallback("image", mouse_crop, ori_image)

    while True:
        i = image.copy()
        key = cv2.waitKey(1) & 0xFF
        if key == ord("r"):
            cv2.destroyWindow("Press 'r' to reframe, press 'c' to save and exit")
            image = ori_image.copy()
        elif key == ord("c"):
            cv2.destroyAllWindows()
            break
        elif not cropping:
            cv2.namedWindow("image", cv2.WINDOW_NORMAL)
            cv2.imshow("image", image)
        elif cropping:
            cv2.rectangle(i, (x_start, y_start), (x_end, y_end), (255, 0, 0), 2)
            cv2.namedWindow("image", cv2.WINDOW_NORMAL)
            cv2.imshow("image", i)
    region = [y_start, y_end, x_start, x_end]
    width, length = image[y_start:y_end, x_start:x_end, 0].shape
    return region, width, length
