##
import time
import cv2
import numpy as np
import serial
import keyboard
from PIL import ImageGrab
from termcolor import colored
from image_manipulation import choose_frame
from functions import *
from datetime import datetime
from datetime import date
import ctypes
import os

#######################################################################################################################
############################################### change parameters here! ###############################################
#######################################################################################################################
port = 'COM4' # port number that needs to be set to connect to function generator

device='round capillary both piezo' # give your device a name
channel_width = 1000 # [um]
percentage = 0.1 # 1 = 100% used for the percentage offset from the theoretical value calculated with the width of the channel
freq_min, freq_max, freq_opt = initialise_frequency(percentage, channel_width) 
freq_min, freq_max = 1450000, 1550000  # [Hz] comment out to set iteration boundaries automatically
subtraction = True  # if no image substraction is wanted, then set substraction = False

# saving files
working_directory = r'C:\NameOfYourWorkingDirectory'  # current working folder location
particle_size = "Fl beads, 600nm " # only used for final summary text
flow_rate = 100 # [ul/min] only used for final summary text

# parameters for phase 1
# all parameters are empirical and need to be adjusted to the setup / experiment
initial_time_delay_1 = 15  # initial time delay for system to settle
frequency_step_size_1 = 100  # frequency step size for phase 1
time_delay_1 = 0.5  # time delay in phase 1
show = True  # showing of "live video" during phase 1

# parameters for phase 2
# all parameters are empirical and need to be adjusted to the setup / experiment
wait_time_transition_2 = 20  # time delay for system to settle
frequency_step_size_2 = -200  # frequency step size for phase 2 (negative value to start iterating downwards)
time_delay_2 = 0.25  # timedelay in phase 2
threshold = 20  # threshold value for change of direction, if it doesn't improve after 20 steps, change direction
swing_value = 0.5  # value with which the stepsize is decreased
freq_bound = 20  # lower frequency boundary for a minimal step size

#######################################################################################################################
#######################################################################################################################
## Phase 0

now_0, today_0, current_time_min_0, current_time_sec_0, time_i_0 = get_time()
working_directory, folder_1, folder_img, folder_plt, folder_data = create_directories(working_directory)

# assert-functions for the function generator
ser = serial.Serial(port, baudrate=9600) # this might need to be changed depending on the function generator
assert check_AFG(ser, port), "Connection-problems with the function generator" # AFG is the name of the function generator
initialise_AFG(ser)

# choose ROI (region of interest)
img = np.array(ImageGrab.grab(bbox=None, all_screens=True, xdisplay=None))  # choose ROI from screen capture
region, width, length = choose_frame(img)

# setting the freq to a neutral state at 10 kHz (audible) and get background image, for background subtraction
set_frequency(ser, 10000)
img = img[:, :, 0]
background_img = get_background(img)  # possible to change the background substraction amount

## Phase 1:

# initialisation of phase 1
now_1, today_1, current_time_min_1, current_time_sec_1, time_i_1 = get_time()
data_1_a = np.empty((int((freq_max - freq_min) // frequency_step_size_1), 2))
freq = freq_min
best_freq_a = freq
best_linewidth_a = 255
set_frequency(ser, freq)
time.sleep(initial_time_delay_1)

# start of finite loop
while freq <= freq_max:

    data_1_a, best_freq_a, best_linewidth_a = get_data_1(region, freq, background_img, data_1_a, best_linewidth_a, best_freq_a,
                                                   subtraction)
    freq += frequency_step_size_1
    set_frequency(ser, freq)
    time.sleep(time_delay_1)

data_1_b = np.empty((int((freq_max - freq_min) // frequency_step_size_1), 2))
best_freq_b = freq
best_linewidth_b = 255

while freq >= freq_min:

    data_1_b, best_freq_b, best_linewidth_b = get_data_1(region, freq, background_img, data_1_b, best_linewidth_b, best_freq_b,
                                                   subtraction)
    freq -= frequency_step_size_1
    set_frequency(ser, freq)
    time.sleep(time_delay_1)

plot_linewidths(data_1_a, data_1_b, folder_1)
best_freq = (best_freq_a + best_freq_b)//2
best_linewidth = (best_linewidth_a + best_linewidth_b)//2

print("Best frequency is:", best_freq)
set_frequency(ser, best_freq)

## Phase 2:
now_2, today_2, current_time_min_2, current_time_sec_2, time_i_2 = get_time()
count = 1
size = 1
freq = best_freq

set_frequency(ser, freq)
time.sleep(wait_time_transition_2)

data_2 = np.empty((12 * threshold, (width + 2)))
data_2, cur_linewidth = get_data_2(region, freq, background_img, data_2, subtraction)
plot_brightness(data_2, size, threshold, folder_plt, folder_data)
display_img(region, background_img, folder_img)

while True:

    if keyboard.is_pressed('q'):
        # save file with used parameters
        now_q, today_q, current_time_min_q, current_time_sec_q, time_i_q = get_time()
        file_name = working_directory + 'summary.txt'  # txt summary of all data
        save_to_file(file_name, now_0, now_1, now_2, now_q, best_freq, freq, freq_min, freq_max, freq_opt,
                     initial_time_delay_1, frequency_step_size_1, time_delay_1, wait_time_transition_2, frequency_step_size_2, time_delay_2, threshold, swing_value,
                     freq_bound, subtraction, particle_size, device, flow_rate)

        # turn off AFG
        """"
        ser.write(b'OUTP1 OFF\n')
        time.sleep(0.1)
        """

        print('Script has finished running, the final frequency tested was (Hertz):', freq)
        break

    # stops display of channel
    if keyboard.is_pressed('n'):
        print('live image stopped')
        cv2.destroyAllWindows()
        show = False

    # restarts display of low fps live video
    if keyboard.is_pressed('s'):
        print('display of channel will re-start')
        show = True

    freq += frequency_step_size_2
    set_frequency(ser, freq)
    time.sleep(time_delay_2)

    data_2, cur_linewidth = get_data_2(region, freq, background_img, data_2, subtraction)

    if size == 12 * threshold:
        pass
    else:
        size += 1

    if cur_linewidth <= best_linewidth:
        best_linewidth = cur_linewidth
        count = 0
    else:
        count += 1

    if count == threshold:
        print(colored("change of direction", 'red'))
        if abs(frequency_step_size_2) >= freq_bound:
            frequency_step_size_2 *= swing_value * (-1)
        else:
            frequency_step_size_2 = frequency_step_size_2 * (-1)

        best_linewidth = cur_linewidth
        count = 0
        plot_brightness(data_2, size, threshold, folder_plt, folder_data)
        if show:
            display_img(region, background_img, folder_img)
