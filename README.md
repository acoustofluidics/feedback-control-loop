# Feedback control loop for optimal piezoelectric excitation frequency

It is recommended to run the code from an environment such as Pycharm Community Edition.
The code is compatible with the arbitrary function generator AFG-2225 from GWInstek.
To use the AFG, the following drivers need to be installed.
- https://www.gwinstek.com/en-global/products/detail/AFG-2225

It is also recommended to use 2 monitors. One that is used to run the code, as the feedback control loop (FCL) outputs plots while it runs, and a second one where the video feed from the camera is displayed. If only one monitor is used, it could be that the plots show up in the portion of the screen from which the image is grabbed, thus resulting in an imprecise input for the FCL. 

Before running the code, all the required libraries (=packages) have to be installed. This can be directly done in the terminal in PyCharm with the command "pip install *package*". The required packages are:
* 'time'
* 'cv2'
* 'numpy as np'
* 'keyboard'
* 'PIL'
* 'termcolor'
* 'image_manipulation'
* 'functions'
* 'datetime'
* 'ctypes'
* 'os'

The time delays, threshold values, swing value, etc. which can also be adapted, should be experimentally determined and are influenced by the particle size, flow rate, etc. In this section also specify if you want to work with a background subtraction (=set subtraction to TRUE, else to FALSE).

FAQ:
- "AttributeError: module 'serial' has no attribute 'Serial'". This error is caused if instead of 'pyserial' the module 'serial' was loaded. Fix it by running 'pip uninstall serial' and then 'pip install pyserial'.
- Program does not respond i.e. in the 'initialise_AFG()' function (no error, but the program is stuck). Try using other 'COM?' ports, as this error is caused by communication issues, meaning the serial module is not connected with the AFG-2225 but tries to exchange information with another USB-device connected to the running computer.

This code was written by Jan Ghadamian and Davide Bernardoni.

For questions or other inquiries please contact hcooper@ethz.ch
